CC = gcc
CFLAGS = -g -Og -Wall -I .

# This flag includes the Pthreads library on a Linux box.
# Others systems will probably require something different.
LIB = -lpthread

all: main

main: main.c
	$(CC) $(CFLAGS) -o main main.c $(LIB)

clean:
	rm -f *.o main *~

